const newsData = [
  {
    month: 'AUG',
    day: '10',
    content: 'Saving Austin’s Water: UT Geoscientists are helping the City of Austin protect its water supply in the face of a changing climate',
    linkTo: "/news-article"
  },

  {
    date: 'AUG 15',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },

  {
    date: 'AUG 10',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  },
  {
    date: 'AUG 15',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit..',
  },
  
  {
    date: 'AUG 15',
    content: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  },
  
  // Add more news items as needed
];

export default newsData;