import React from 'react';
import './ScrollButton.css'; // Import your button styles

function ScrollButton({ buttonText, targetSection }) {
  const handleClick = () => {
    const section = document.getElementById(targetSection);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <button className="scroll-button" onClick={handleClick}>
      {buttonText}
    </button>
  );
}

export default ScrollButton;
