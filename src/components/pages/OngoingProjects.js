import React, { useEffect, useState } from 'react';
import ResearchCardGrid from '../ResearchCardGrid';
import Footer from '../Footer';
import Content from '../Content';
import './OngoingProjects.css';
import { Link } from 'react-router-dom';
import yaml from 'js-yaml'; // Import js-yaml library

function OngoingProjects() {
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    // Specify the path to your YAML file
    const yamlFilePath = process.env.PUBLIC_URL + '/Projects.yaml';

    // Use fetch to load the YAML file
    fetch(process.env.PUBLIC_URL + '/Projects.yaml')
      .then((response) => response.text())
      .then((yamlData) => {
        // Parse the YAML data into an array of objects
        const parsedProjects = yaml.load(yamlData); // Use loadAll to parse multiple documents

        setProjects(parsedProjects);
      })
      .catch((error) => {
        console.error('Error fetching or parsing YAML file:', error);
      });
  }, []);

  return (
    <>
      <div className="cardsgrid">
        <div className="wall-container">
          <img className="bg-img" src={process.env.PUBLIC_URL + '/images/UT_Picture.jpg'} />
        </div>
        <h1>Ongoing Projects</h1>
        <p>Description for Ongoing Projects</p>
        <ResearchCardGrid cards={projects} />
      </div>

      <div className="ongoing-projects-page">
        {projects.map((project, index) => (
          <div key={index}>
            <Content
              title={project.title}
              pictureSrc={project.pictureSrc}
              content={project.longContent}
              targetSection={project.targetSection}
            />
            <Link to={`/content/${project.targetSection}`}>Read More</Link>
          </div>
        ))}
      </div>

      <Footer />
    </>
  );
}

export default OngoingProjects;