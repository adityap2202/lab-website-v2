import React from 'react';
import NewsCard from './NewsCard'; // Import the NewsCard component
import newsData from './NewsData'; // Import the news data

function NewsSection() {
  return (
    <div className="news-section">
      {newsData.map((newsItem, index) => (
        <React.Fragment key={index}>
          <NewsCard month={newsItem.month} day = {newsItem.day} content={newsItem.content} />

        </React.Fragment>
      ))}
    </div>
  );
}

export default NewsSection;