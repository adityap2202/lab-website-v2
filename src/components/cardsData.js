
export const cards = [
  {
    image: process.env.PUBLIC_URL + "/images/thermal-1.jpg",
    title: "Extreme Heat",
    content: "Paragraph and info 1",
    link: "/link1",

  },

  {
    image: process.env.PUBLIC_URL + "/images/AQI.jpg",
    title: "Air Quality",
    content: "Paragraph and info 1",
    link: "/link1",

  },

  {
    image: process.env.PUBLIC_URL + "/images/precipitation.png",
    title: "Precipitation",
    content: "Paragraph and info 1",
    link: "/link1",

  },



  // Add more cards here
];