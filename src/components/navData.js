
export const navLinks = [
  {
    label: 'HOME',
    path: '/',
    sublinks: [],
  },
  {
    label: 'RESEARCH',
    path: '/projects/OngoingProjects',
    sublinks: [
      {
        label: 'Ongoing Projects',
        path: '/projects/OngoingProjects',
        innerSublinks: [
          {
            label: 'Cool Pavement',
            path: '/projects/OngoingProjects/',
          },
          {
            label: 'Community Heat Mapping',
            path: '/projects/extreme-heat/community-heat-mapping',
          },
          // Add more sublinks as needed
        ],
      },
      {
        label: 'Completed Projects',
        path: '/projects/precipitation',
        innerSublinks: [
          {
            label: 'Project 2 Detail 1',
            path: '/projects/precipitation/detail1',
          },
          {
            label: 'Project 2 Detail 2',
            path: '/projects/precipitation/detail2',
          },
          // Add more sublinks as needed
        ],
      },
      
      // Add more top-level project links as needed
    ],
  },
  {
    label: 'NEWS',
    path: '/research',
    sublinks: [],
  },
  {
    label: 'PEOPLE',
    path: '/people',
    sublinks: [],
  },
  {
    label: 'PUBLICATIONS',
    path: '/publications',
    sublinks: [],
  },
  {
    label: 'CONTACT',
    path: '/contact',
    sublinks: [],
  },
];