import React from 'react';
import './ResearchCards.css';
import ScrollButton from './ScrollButton';
import { Button } from './Button';

function ResearchCard({ title, pictureSrc, content, buttonText, linkTo, targetSection }) {

  console.log("PictureSec" + pictureSrc)

  const handleClick = () => {
    const section = document.getElementById(targetSection);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (

    <div className="cards">
      <h2 className="card-title">{title}</h2>
      <img src={process.env.PUBLIC_URL+ pictureSrc} alt={title} className="card-picture" />
      <p className="card-content">{content}</p>
      <Button buttonStyle="btn--primary" buttonSize="btn--large" to={`/content/${targetSection}`}>
              Learn More
            </Button>
       {/*<ScrollButton buttonText={buttonText || "Know More"} targetSection={targetSection} />*/}
    </div>
  );
}

export default ResearchCard;