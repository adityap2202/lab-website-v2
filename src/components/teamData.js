// teamData.js

export const principalInvestigators = [
  {
    name: "Dev Niyogi",
    title: "Professor and William Stamps Farish Chair in Geosciences",
    image: process.env.PUBLIC_URL + "/images/Dev.jpg",
  },
  // Add more PI members here
];

export const postdoctoralResearchers = [
  {
    name: "Naveen Sudharshan",
    image:process.env.PUBLIC_URL +"/images/Naveen.jpg",
  },
  {
    name: "Manmeet Singh",
    image: process.env.PUBLIC_URL + "/images/Manmeet.jpg",
  },

  // Add more postdoc members here
];


export const Staff
 = [
  {
    name: "Allysa Dallmann",
    image:process.env.PUBLIC_URL +"/images/Naveen.jpg",
  },
  {
    name: "Aditya Patel",
    image: process.env.PUBLIC_URL + "/images/Adi.jpg",
  },

  // Add more postdoc members here
];

export const Scientists
 = [
  {
    name: "Hassan Dashtian",
    image:process.env.PUBLIC_URL +"/images/Naveen.jpg",
  },
  {
    name: "Unnikrishna",
    image: process.env.PUBLIC_URL + "/images/Naveen.jpg",
  },

  // Add more postdoc members here
];

export const graduateStudents = [
  {
    name: "Trevor Brooks",
    image: process.env.PUBLIC_URL + '/images/Trevor.jpg',
  },

  {
    name: "Harsh Kamath",
    image: process.env.PUBLIC_URL + '/images/Harsh.jpg',
  },

  {
    name: "Arya Chavoshi",
    image: process.env.PUBLIC_URL + '/images/Arya.jpg',
  },

  {
    name: "Ting-Yu Dai",
    image: process.env.PUBLIC_URL + '/images/Ting-Yu.jpg',
  },

  {
    name: "Xinxin Sui",
    image: process.env.PUBLIC_URL + '/images/Sui_2.jpg',
  },

  {
    name: "Alka Tiwari",
    image: process.env.PUBLIC_URL + '/images/Alka.jpeg',
  },

  {
    name: "Varsha Revandkar",
    image: process.env.PUBLIC_URL + '/images/Varsha.jpg',
  },

  {
    name: "Caleb Adams",
    image: process.env.PUBLIC_URL + '/images/Caleb.jpeg',
  },

  {
    name: "Sasanka Talukdar",
    image: process.env.PUBLIC_URL + '/images/Sasanka.jpg',
  },
  // Add more graduate student members here
];


export const Undergrads = [
  {
    name: "Sonja Rebarbn",
    image:process.env.PUBLIC_URL +"/images/Sonja.jpg",
    title: "Swarthmore College"
  },
  {
    name: "Maya Niyogi",
    image: process.env.PUBLIC_URL + "/images/Sonja.jpg",
    title:"Johns Hopkins University"
  },

  {
    name: "Sarah Steele",
    image: process.env.PUBLIC_URL + "/images/Sarah.jpg",
  },

  {
    name: "Hiruni Dissanayake",
    image: process.env.PUBLIC_URL + "/images/Hiruni.jpg",
  },


  {
    name: "Matthew Nattier",
    image: process.env.PUBLIC_URL + "/images/Manmeet.jpg",
  },


  // Add more postdoc members here
];

export const Visitors
 = [
  {
    name: "Arushi Vashisht",
    image:process.env.PUBLIC_URL +"/images/Manmeet.jpg",
  },
  {
    name: "Maria Stan",
    image: process.env.PUBLIC_URL + "/images/Naveen.jpg",
  },

  // Add more postdoc members here
];