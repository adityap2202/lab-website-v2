import React from 'react';
import '../App.css';
import { Button } from './Button';
import './HeroSection.css';
import VideoCarousel from './VideoCarousel';
import VideoSlider from './VideoSlider';
const videos = [
  { src: process.env.PUBLIC_URL + '/videos/drone-video.mp4', title: 'TExUS LAB', subtitle: "Welcome to the" },
  { src: process.env.PUBLIC_URL + '/videos/hurricane-video-1.mp4', thirdtext: ' We investigate the roles and interactions between extreme weather and climate with urban environments.' },
  { src: process.env.PUBLIC_URL + '/videos/Geoscience_Hurricane.mp4', thirdtext: ' Our goal is to be at the forefront of urban climate and weather research and to better understand and prepare for a changing future' },
  { src: process.env.PUBLIC_URL + '/videos/Austin_skyline_zooming.mp4', thirdtext: 'We are a part of the Department of Earth and Planetary Sciences at The University of Texas at Austin',logo: process.env.PUBLIC_URL + '/images/knockout_university_primary.png'  },
  // Add more video sources as needed
];

function HeroSection() {
  return (
    <div className="hero-container">
      
      <div className="hero-slider">
        <VideoSlider videos={videos} />
      </div>
    </div>
  );
}


export default HeroSection;
