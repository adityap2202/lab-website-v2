import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import React, { useEffect, useState } from 'react';
import People from './components/pages/People';
import OngoingProjects from './components/pages/OngoingProjects';
import { navLinks } from './components/navData';
import ContentPage from './components/ContentPage';
import yaml from 'js-yaml'; // Import js-yaml library
import PeopleDetails from './components/PeopleDetails'
import { principalInvestigators, postdoctoralResearchers, Staff, Scientists, graduateStudents, Undergrads, Visitors } from './components/teamData'; // Import your data


function App() {
  const [projects, setProjects] = useState([]);
  const [peopleProjects, setPeopleProjects] = useState({});


  useEffect(() => {
    // Fetch the YAML data from your file
    fetch(process.env.PUBLIC_URL + '/Projects.yaml')
      .then((response) => response.text())
      .then((yamlData) => {
        // Parse the YAML data into an array of objects
        const parsedProjects = yaml.load(yamlData);

        setProjects(parsedProjects);

        // Create a data structure that associates people with projects
        const peopleProjectsData = {};
        parsedProjects.forEach((project) => {
          const collaborators = project.Collaborators.split(',').map((name) => name.trim());
          collaborators.forEach((collaborator) => {
            // Replace spaces in names with underscores and convert to lowercase
            const formattedName = collaborator.replace(/\s+/g, '_').toLowerCase();
            if (!peopleProjectsData[formattedName]) {
              peopleProjectsData[formattedName] = [];
            }
            peopleProjectsData[formattedName].push(project);
          });
        });
        setPeopleProjects(peopleProjectsData);
      })
      .catch((error) => {
        console.error('Error fetching or parsing YAML file:', error);
      });

    window.scrollTo(0, 0);

    console.log(peopleProjects                                                    )
  }, []);

  return (
    <>
      <Router>
        <Navbar navLinks={navLinks} />
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/lab-website-v2" exact element={<Home />} />

          <Route path="/projects/OngoingProjects" element={<OngoingProjects />} />
          {projects.map((project, index) => {
            console.log(`Generating route for ${project.targetSection}`);
            return (
              <Route
                key={index}
                path={`/content/${project.targetSection}`}
                element={<ContentPage project={project} />}
              />
            );
          })}

          <Route path="/People" element={<People />} />
          {[
            ...principalInvestigators,
            ...postdoctoralResearchers,
            ...Staff,
            ...Scientists,
            ...graduateStudents,
            ...Undergrads,
            ...Visitors,
          ].map((person, index) => (
            <Route
              key={index}
              path={`/people/${person.name.replace(/\s+/g, '_').toLowerCase()}`}
              element={<PeopleDetails person={person} peopleProjects={peopleProjects} />}
            />
          ))}


        </Routes>
      </Router>
    </>
  );
}

export default App;